import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ConteudoPrincipalComponent } from './conteudo-principal/conteudo-principal.component';
import { ConteudoSecundarioComponent } from './conteudo-secundario/conteudo-secundario.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ConteudoPrincipalComponent,
    ConteudoSecundarioComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
